#Escreva um programa que dado um inteiro n positivo calcula e imprime a soma de todos os numeros
#inteiros entre 1 e n.

n = int(input("Digite um numero: "))
i = 1;
soma = 1;
while i < n:
    print(i , "+", end=" ")
    i = i + 1
    soma += i;
print(i, "=", soma)