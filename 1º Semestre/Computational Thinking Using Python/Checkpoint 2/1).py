n = int(input("Digite a quantidade de números da lista: ")) 

while n < 1: #Enquanto a quantidade de numeros for menor que 1, faça:
    n = int(input("Numéro inválido para realizar resolução, Digite um inteiro positivo: "))

m = 0
qtd_seq = 0
anterior = 0 
num = 0

print("Digite a lista:")
while n > m:
    anterior = num
    num = int(input()) #Digite os numeros para saber quantas vezes ele repete.
    qtd_seq += 1
    if anterior == num: # se anterior for igual ao num digitado
        qtd_seq -= 1   #Adicionar na variavel qtd_seq (quantidade de sequencia!)
    m +=1

print("A quantidade de sequências é de: ",qtd_seq) #Exibe quantidade