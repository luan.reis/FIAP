n = int(input("Digite o número de produtos: "))
m = 1
comp_por = float(0) #variavel para comparar a porcentagem
comp_vlr = float(0) #variavel para comparar o valor

#Caso o usuario entre com um numero menor oou igual a zero para 'n'
while n <= 0 :
    n = int(input("Valor inválido,por favor digite o número de produtos: "))


while m <= n :
    vlr_antigo = float(input("Digite o antigo preço do produto: "))
    vlr_novo = float(input("Digite o novo preço do produto: "))


    por_aumento = vlr_novo * 100 / vlr_antigo - 100 #Calculo da porcentagem de aumento
    vlr_aumento = vlr_novo - vlr_antigo #Calculo do almento de valor

    if por_aumento > comp_por :
        comp_por = por_aumento #O comparador irá receber a maior porcentagem ja listada
        prod_por = m #a variavel 'prod_por' vai receber o numero do produto com a maior porcentagem de aumento

    if vlr_aumento > comp_vlr :
        comp_vlr = vlr_aumento #O comparador irá receber o maior valor de aumento ja listada
        prod_vlr = m #a variavel 'prod_vlr' vai receber o numero do produto com o maior aumento de valor

    m += 1

print("O", prod_por, "º produto foi o que teve maior aumento de porcentagem, que foi de", comp_por, "%")
print("O", prod_vlr, "º produto foi o que teve maior aumento de valor, que foi de R$", comp_vlr)