# a nossa base de numera¸c˜ao ´e a decimal, ou seja, os d´ıgitos
# para representar todos os n´umeros v˜ao de 0 at´e 9
# I j´a os computadores usam a base bin´aria de numera¸c˜ao, ou
# seja, eles entendem apenas o 0 ou o 1 (apagado e aceso)
# I em algumas situa¸c˜oes, o computador usa tamb´em a base
# hexadecimal (16) de 0 a 9 e de A a F
# I nos pr´oximos eslaides veremos como fazer a convers˜ao de
# # n´umero bin´ario para decimal e vice-versa


# 1 · 10 ** 4 + 0 · 10 ** 3 + 2 · 10 ** 2 + 4 · 10 ** 1 + 7 · 10**0
    #   dm         m         c         d          u

# dm = dezena de milhar
# m = milhar 
# c = centena 
# d = dezena
# u = unidade

# 64 | 32 | 16  | 8  | 4  | 2  | 1  |
# 1  |  0 |  1  | 1  | 1  | 0  | 0  |

