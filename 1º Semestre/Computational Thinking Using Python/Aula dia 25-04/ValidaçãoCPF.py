# O CPF ´e composto por 11 d´ıgitos sendo que os 2 ´ultimos s˜ao
# chamados de d´ıgitos de controle. Representando os d´ıgitos do CPF
# por letras (ABC.DEF.GHI-JK), podemos descrever o algoritmo que
# gera os d´ıgitos de controle do seguinte modo:

# primeiramente vamos gerar o d´ıgito correspondente `a letra J
# I vamos multiplicar cada um dos d´ıgitos ABCDEFGHI por 10,
# 9, 8, 7, 6, 5, 4, 3 e 2
# I soma = 10*A + 9*B + 8*C + 7*D +
# 6*E + 5*F + 4*G + 3*H + 2*I
# I a seguir pegamos o valor da soma e calculamos o resto da
# divis˜ao por 11, resto = soma % 11
# I se o resto < 2 ent˜ao o primeiro d´ıgito verificador ´e 0, sen˜ao
# ele vale 11 - resto

# 487.902.700-61



