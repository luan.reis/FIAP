# Dada uma sequência de números inteiros onde o último elemento é o 0, escreva um algoritmo
# que calcula a soma dos números pares da sequência.

num = int(input("Digite um numero inteiro para fazer a soma de todos numeros pares: "));
soma = 0
i = 1

while i <= num:
    if i % 2 == 0:
        print(i)
        soma += i

    i += 1
print("A soma dos valores pares é: ", soma)