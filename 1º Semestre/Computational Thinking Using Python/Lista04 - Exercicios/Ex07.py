# A conversão de graus Fahrenheit para centígrados é obtida pela fórmula C =
# 5
# 9
# (F − 32).
# Escreva um algoritmo que calcule e escreva uma tabela de graus centígrados em função de
# graus Fahrenheit que variem de 50 a 150 Fahrenheit de 1 em 1.

f = 50;
c = 0.0;


while f <= 150:
    c = (f - 32) * 5/9
    print("Fahrenheit:",f, " = Cº:",round(c,1))
    f += 1;
