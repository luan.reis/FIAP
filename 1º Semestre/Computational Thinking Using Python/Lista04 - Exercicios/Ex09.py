# Dados um montante em dinheiro inicial d, uma taxa de juros mensal j e um período de
# tempo em meses t, escreva um algoritmo que calcula o valor nal em dinheiro se d car
# aplicado a taxa de juros j durante t meses.

print("============= Calculadora de investimento =============")
d =float(input("Digite um valor R$ inicial: "))
j= float(input("Informe a taxa mensal: "))
t= int(input("Informe a quantidade de meses que irá aplicar: "))

j = j / 100

montante = d * (1 + j) ** t


print("R$", round(montante,2))