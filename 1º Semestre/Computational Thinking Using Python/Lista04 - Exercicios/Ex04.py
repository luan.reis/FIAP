# Dados n um inteiro positivo e uma sequência de n números reais, escreva um algoritmo que
# conta e imprime a quantidade de números positivos e a quantidade de números negativos.


vezes = int(input("Digite a quantidade vezes que quer digitar: "))
i = 1;
negativo = 0;
positivo = 0;

while i <= vezes:
    num = float(input("Digite o {}º numero: ".format(i)))
    if num < 0:
        negativo += 1;
        print(num, "Sou um número negativo")
    else:
        positivo += 1;
        print(num, "Sou um número positivo")
    i+= 1;
print("Quantidade de numeros positivos digitados:{}".format(positivo))
print("Quantidade de numeros negativos digitados:{}".format(negativo))