# Escreva uma função em Python que recebe como parâmetros uma String e retorna a
# quantidade de vogais existentes nessa String.
# A, E, I, O, U.


def Vogais(palavra, vogais): 
    palavra = palavra.casefold() 
    count = {}.fromkeys(vogais, 0) 
    for i in palavra: 
        if i in count: 
            count[i] += 1   
    return count 
      
vogais = 'aeiou'

palavra = str(input("digite uma palavra: "))


print(Vogais(vogais, palavra))