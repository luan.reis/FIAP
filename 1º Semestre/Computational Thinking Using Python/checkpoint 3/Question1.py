# Você foi contratado para elaborar um algoritmo que irá auxiliar o inventário de uma
# loja. Seu algoritmo deverá ler uma sequência de informação de produtos. Uma informação
# de produto é composta pelos seguintes dados:
#  produto (string)
#  quantidade (inteiro)
#  preço (real)
# A sequência das informações de produtos é nalizada quando é digitada uma quantidade
# negativa. Sua tarefa é encontrar o nome do produto com o maior número de itens e o nome
# do produto com o maior valor agregado do estoque (preço x quantidade).
# Note que, as três informações do produto são inseridas individualmente.

qtdM = precM = 0
prodQ = prodQP = "" 

while True:
    prd = str(input("Digite o nome do produto: "))
    qtd = int(input("Digite a quantidade: "))

    if(qtd < 0):
        print("Programa encerrado")
        break;
    prec = float(input("Digite o preço: "))

    
  
    
    if(qtd > qtdM):
        qtdM = qtd
        prodQ = prd
    
    if((qtd * prec) > precM):
        precM = (qtd * prec)
        prodQP = prd

print("="*30, "Resultado","="*30)
if(qtdM == 0 and precM == 0 ):
    print("Não houve entrada de informações suficiente")
else:
    print(f"O produto com maior quantidade {prodQ} com: {qtdM}")
    print(f"O produto com maior valor agregado {prodQP} com: {precM}")