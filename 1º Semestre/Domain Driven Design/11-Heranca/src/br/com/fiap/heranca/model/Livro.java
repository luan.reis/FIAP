package br.com.fiap.heranca.model;

//Object <- Produto <- Livro

//Heran�a -> O livro � um produto? � um?
//Atributo -> O livro tem uma editora? Tem um?
public class Livro extends Produto {

	// Atributos
	private String genero;
	private int numeroPaginas;
	
	//Construtor
	public Livro(int codigo, String titulo, String material, 
			double preco, String genero, int numeroPaginas) {
		super(codigo, titulo, material, preco);
		this.genero = genero;
		this.numeroPaginas = numeroPaginas;
	}
	
	//M�todo
	public boolean validarEnvioEletronico() {
		//Valida se o material tem a palavra "digital"
//		if (material.contains("digital")) {
//			return true;
//		} 
//		return false;
		return material.contains("digital");
	}

	//CTRL + 3 -> ggas
	// Getters e Setters
	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(int numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}
}