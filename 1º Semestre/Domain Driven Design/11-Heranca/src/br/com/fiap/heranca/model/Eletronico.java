package br.com.fiap.heranca.model;

//Heran�a: Object <- Produto <- Eletronico
public class Eletronico extends Produto {

	//Atributos
	private int voltagem;
	
	private boolean bivolt;
	
	//Construtores
	public Eletronico(int codigo, String titulo, String material,
			double preco, int voltagem, boolean bivolt) {
		super(codigo, titulo, material, preco);
		this.bivolt = bivolt;
		this.voltagem = voltagem;
	}
	
	//Getters e Setters
	public int getVoltagem() {
		return voltagem;
	}

	public void setVoltagem(int voltagem) {
		this.voltagem = voltagem;
	}

	public boolean isBivolt() {
		return bivolt;
	}

	public void setBivolt(boolean bivolt) {
		this.bivolt = bivolt;
	}
	
}