package br.com.fiap.polimorfismo.model;

public class Alimento extends Produto {

	private String tipo;
	
	private boolean industrializado;
	
	private boolean refrigerado;
	
	

	public Alimento(String tipo, double preco, boolean industrializado, boolean refrigerado) {
		this.preco = preco;
		this.tipo = tipo;
		this.industrializado = industrializado;
		this.refrigerado = refrigerado;
	}
	
	public Alimento() {
		
	}
	
	@Override
	public String toString() {
		return "\nAlimento \n" +  tipo + "\n" + preco + "\ninstrualizado: " + industrializado + "\nrefrigerado: " + refrigerado;
	}
	



	@Override
	public double calcularDesconto(String cupom) {
		if(cupom.equalsIgnoreCase("FIAP40")) {
			return preco * 0.6;
		}
		return super.calcularDesconto(cupom);
	}

	
	@Override
	public double calcularDesconto() {
		return preco * 0.9; //valor padr�o de desconto
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean isIndustrializado() {
		return industrializado;
	}

	public void setIndustrializado(boolean industrializado) {
		this.industrializado = industrializado;
	}

	public boolean isRefrigerado() {
		return refrigerado;
	}

	public void setRefrigerado(boolean refrigerado) {
		this.refrigerado = refrigerado;
	}
	
}