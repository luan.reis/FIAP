package br.com.fiap.tds.ltp.ex2.produtos;

import br.com.fiap.tds.ltp.ex2.produtos.livro.Livro;

public class LivroInfantil extends Livro {

	private String ilustrador;

	public String getIlustrador() {
		return ilustrador;
	}

	public void setIlustrador(String ilustrador) {
		this.ilustrador = ilustrador;
	}
	
}