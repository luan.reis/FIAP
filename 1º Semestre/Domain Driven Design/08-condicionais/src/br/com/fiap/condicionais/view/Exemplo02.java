package br.com.fiap.condicionais.view;

import javax.swing.JOptionPane;

public class Exemplo02 {

	public static void main(String[] args) {
		// Criando calculadora

		// Pedir a opera��o para usuario ( +, -, *, /)

		// Realizar e exibir a resposta

		int num1 = Integer.parseInt(JOptionPane.showInputDialog("Informe um numero: "));
		int num2 = Integer.parseInt(JOptionPane.showInputDialog("Informe o segundo numero: "));

		String opcao = JOptionPane.showInputDialog("Qual opera��o voc� quer fazer [ + ][ - ][ * ][ / ]");

		switch (opcao) {
		case "+":
			int soma = num1 + num2;
			JOptionPane.showMessageDialog(null, "A soma: " + num1 + " + " + num2 + " = " + soma);
			break;
		case "-":
			int sub = num1 - num2;
			JOptionPane.showMessageDialog(null, "A subtra�ao: " + num1 + " - " + num2 + " = " + sub);
			break;
		case "*":
			int multi = num1 * num2;
			JOptionPane.showMessageDialog(null, "A multiplica��o: " + num1 + " * " + num2 + " = " + multi);
			break;
		case "/":
			if (num2 == 0) {
				JOptionPane.showMessageDialog(null, "IMPOSSIVEL DIVIDIR POR 0");
				break;
			}
			double div = (double) num1 / (double) num2;
			JOptionPane.showMessageDialog(null, "A divis�o: " + num1 + " / " + num2 + " = " + div);
			break;
		default:
			JOptionPane.showMessageDialog(null, "Voc� digitou a op��o incorreta");

		}

	}
}
