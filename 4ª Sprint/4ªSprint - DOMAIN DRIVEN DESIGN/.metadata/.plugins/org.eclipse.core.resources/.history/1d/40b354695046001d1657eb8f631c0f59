package br.com.merge.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.merge.excetion.IdNotFoundException;
import br.com.merge.model.Candidato;

/**
 *Classe responsável por cadastrar, encontrar, alterar senha e listar um Candidato
 *@author Henrique Cesar
 *@author Dennys Nascimenro 
 *@author Luan Reis
 *@author Gustavo Fonseca
 *
 */

public class CandidatoDao {
	
	private Connection conexao; 
	
	/**
	 * Coleção que armazena os Candidatos da aplicacao
	 */
	List<Candidato> listaCandidato = new ArrayList<>();
	/**
	 * Método que cadastra um candidato
	 * @param candidato 
	 * @param email do candidato
	 * @return se esse emai já está cadastrado ou se foi cadastrado
	 * @throws SQLException 
	 */
	public void cadastrar(Candidato candidato) throws SQLException {
	
		PreparedStatement stmt = conexao.prepareStatement("insert into T_MERGE_CANDIDATO values "
				+ "(sq_t_merge_candidato.nextval, ?, ?, to_date(?,'dd/mm/yyyy'), ?, ? )"
				, new String[] {"id_candidato"});
		
		stmt.setString(1, candidato.getNome()); 
		stmt.setString(2, candidato.getCpf());
		stmt.setString(3,candidato.getDtNascimento());
		stmt.setString(4, candidato.getDsEstadoCivil());
		stmt.setString(5, candidato.getDsSexo());
		
		stmt.executeUpdate();
		
		ResultSet result = stmt.getGeneratedKeys();
		if (result.next()) {
			int codigo = result.getInt(1);
			candidato.setCodigo(codigo);
		}
	}
//
//	/**
//	 * Método que encontra na lista o candidato pelo nome
//	 * @param nome completo do candidato
//	 * @return 
//	 */
//	public Candidato encontrarPorNome(String nomeCompleto) {
//
//		for (int i = 0; i < listaCandidato.size(); i++) {
//			if (listaCandidato.get(i).getNome().equalsIgnoreCase(nomeCompleto)) {
//				listaCandidato.get(i);
//			}
//		}
//		return null;

//	}

	/**
	 * Método que encontra na lista o candidato pelo cpf
	 * @param cpf dop candidato
	 * @return o candidato 
	 * @throws SQLException 
	 * @throws IdNotFoundException 
	 */
	public Candidato encontrarPorCpf(String cpf) throws SQLException, IdNotFoundException {
		
		PreparedStatement stmt = conexao.prepareStatement("select * from T_MERGE_CANDIDATO where NR_CPF = ?");
		
		stmt.setString(1, cpf);
		
		ResultSet result = stmt.executeQuery();
		
		if(!result.next()) {
			throw new IdNotFoundException("Candidato não encontrado");
		}
		int codigo = result.getInt("ID_CANDIDATO");
		String nome = result.getString("NM_CANDIDATO");
		String dataNascim = result.getString("DT_NASCIMENTO");
		String estadoCivil = result.getString("DS_ESTADO_CIVIL");
		String sexo = result.getString("DS_SEXO");
		
		
		Candidato candidato = new Candidato(codigo, nome, cpf, dataNascim, estadoCivil, sexo);		
		return candidato;
	}
	/**
	 * Método que altera a senha do login do candidato
	 * @param candidato 
	 * @param nova senha
	 * @return se a senha nova se repete com a atual ou se foi alterado
	 */
	public String alterarSenha(Candidato candidato, String novaSenha) {
		if (candidato.getPsSenhaLogin().equals(novaSenha)) {
			return "A senha não pode ser altera para a atual";
		}
		candidato.setPsSenhaLogin(novaSenha);
		return "Senha alterada com sucesso!";
	}
	/**
	 * Lista os candidatos
	 * @return a lista de candidatos
	 * @throws SQLException 
	 */
	public List<Candidato> listar() throws SQLException {
		PreparedStatement stmt = conexao.prepareStatement("SELECT * FROM T_MERGE_CANDIDATO");
		ResultSet result = stmt.executeQuery();		
		List<Candidato> lista = new ArrayList<Candidato>();
		
		while(result.next()) {
			
			int codigo = result.getInt("ID_CANDIDATO");
			String nome = result.getString("NM_CANDIDATO");
			String cpf = result.getString("NR_CPF");
			String dataNascim = result.getString("DT_NASCIMENTO");
			String estadoCivil = result.getString("DS_ESTADO_CIVIL");
			String sexo = result.getString("DS_SEXO");
			
			Candidato candidato = new Candidato(codigo, nome, cpf, dataNascim, estadoCivil, sexo);
			
			lista.add(candidato);
		}
		
		return lista;
	}
}
