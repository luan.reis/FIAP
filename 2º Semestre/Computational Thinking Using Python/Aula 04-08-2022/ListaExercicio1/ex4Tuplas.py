# Faça uma função que recebe uma tupla de números reais e retorna a média aritmética de
# todos os valores contidos dentro da tupla.

def verifMedia(dados):
    media = 0

    for num in dados:
        media += num

    return media / len(dados)    


conjunto = (10, 10, 10, 10 )
print(verifMedia(conjunto))