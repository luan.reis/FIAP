def fatorial(n):
    if n == 0:
        return 1
    else:
        return n * fatorial(n - 1)


print("Fatorial: ", fatorial(23))


def fibonacci(n):
    if n == 1 or n == 2:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)

print("Fibonnaci: ", fibonacci(10))