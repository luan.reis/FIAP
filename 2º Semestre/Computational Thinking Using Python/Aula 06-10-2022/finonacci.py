def fibonacci(n):
    if n == 1 or n == 2:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)


def fibo(n):
    ant_a = 1
    ant_b = 1
    atual: 1

    while n > 2:
        atual = ant_a + ant_b
        ant_b = ant_a
        ant_a = atual
        n = n - 1
        return atual


fibonacci(40)