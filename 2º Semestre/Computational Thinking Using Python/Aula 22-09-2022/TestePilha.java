public class TestePilha {

    public static void main(String[] args) {
        
        String frase = "O futuro não importa, o que importa é daqui para frente";

        Pilha<String> pilha = new Pilha<>(20);

        String[] palavras = frase.split(" ");

        for(String s : palavras){

            if(!pilha.estaCheia()){
                pilha.empilha(s);
            }else{
                System.out.println("Stack overflow!");
                System.exit(42);
            }

        }

        while(!pilha.estaVazia()){
            String s = pilha.desempilha();
            System.out.println(s);
        }

    }

}
