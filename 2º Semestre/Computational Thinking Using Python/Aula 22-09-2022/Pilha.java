public class Pilha<E>{

    private Object[] vetor;
    private int topo;

    public Pilha(int capacidade){
        vetor = new Object[capacidade];
        topo = -1;
    }

    public void empilha(E info){
        topo++;
        vetor[topo] = info;

    }

    public boolean estaVazia(){
        if(topo == -1){
            return true;
        }
        return false;
    }

    public boolean estaCheia(){
        if(topo == vetor.length - 1) {
            return true;
        }
        return false;
    }

    public E desempilha(){
        return (E)vetor[topo--];
    }

    public E topoPilha(){
        return (E)vetor[topo];

    }



}