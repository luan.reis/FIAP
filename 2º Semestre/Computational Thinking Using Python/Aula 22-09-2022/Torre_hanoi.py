from stack import Pilha


def pega_pilha(nome, a, b, c):
    if (nome == 'A'):
        return a
    elif nome == 'b':
        return b
    else:
        return c


def move(orig, dest, a, b, c):
    pilha_o = pega_pilha(orig, dest, a, b, c)
    pilha_d = pega_pilha(dest, a, b, c)

    if orig == dest:
        print("Movimento invalido")
        return False
    elif pilha_o.isEmpty():
        print('Movimento invalido')
        return False
    elif not pilha_d.isEmpty() and pilha_o.peek() > pilha_d.peek():
        print("Movimento invalido")
        return False
    
    info = pilha_o.pop()
    pilha_d.put(info)
    print(orig, "->", dest, "disco:", info)



a = Pilha()
b = Pilha()
c = Pilha()

a.put(3)
a.put(2)
a.put(1)

contador = 0;

origem = input("Pilha de origem: ")
destino = input("Pilha destino: ")

if move(origem, destino, a, b, c) == True:
    if destino == 'c':
        contador = contador + 1
    elif origem == 'c':
        contador = contador - 1
