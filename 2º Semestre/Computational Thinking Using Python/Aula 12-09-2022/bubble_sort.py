def subir(lista):
    pos = len(lista) - 1
    while pos > 0:
        if lista[pos-1] > lista[pos]:
            aux = lista[pos]
            lista[pos] = lista[pos-1]
            lista[pos-1] = aux
        pos = pos - 1
        
def bubble_sort(lista):
    for i in range(len(lista)):
        subir(lista) 

vetor = [44, 30, 18, 23, 9, 16, 5, 4, 0]
bubble_sort(vetor)
print(vetor)               