import velha
tab = velha.criaTabuleiro()
jogador = 'X'

while velha.temEspaco(tab) and not velha.haGanhador(tab):
    velha.imprime(tab)

    print("Vez do jogador: ", jogador)
    lin = int(input( "Linha: "))
    col = int(input( "Coluna: "))

    resp = velha.joga(tab, lin, col, jogador)

    if(resp == True):
        jogador = velha.trocaJogador(jogador)
    else:
        print("jogada invalida, digite outra posição")

jogador= velha.trocaJoador(jogador)

if(velha.haGanhador(tab)):
    print(jogador, "Ganhou!!")
else:
    print("Deu velha")