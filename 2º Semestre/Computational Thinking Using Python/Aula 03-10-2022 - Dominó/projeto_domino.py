import random


def cria_domino():
    lista = []

    j = 0
    while j <= 6:
        i = j
        while i <= 6:
            lista.append([j, i])
            i += 1
        j += 1
    return lista


def mistura(lista):
    for i in range(100):
        x = random.randint(0, 27)
        y = random.randint(0, 27)
        aux = lista[x]
        lista[x] = lista[y]
        lista[y] = aux


def distribui(lista, qtd):
    mao = []
    while qtd > 0:
        pedra = lista.pop()
        mao.append(pedra)
        qtd = qtd - 1
    return mao


def joga_usuario(mao, pontas):
    print("pedras: ", mao)
    print("pontas: ", pontas)
    pos = int(input("qual pedra deseja jogar? "))
    return mao.pop(pos)


def adiciona_jogada(pedra, mesa, pontas):
    if len(mesa) == 0:
        mesa.append(pedra)
        pontas[0] = pedra[0]
        pontas[1] = pedra[1]
        return True

    valor_0 = pedra[0]
    valor_1 = pedra[1]

    if pontas[0] == valor_0:
        mesa.insert(0, pedra)
        pontas[0] = valor_1
        return True
    elif pontas[0] == valor_1:
        mesa.insert(0, pedra)
        pontas[0] = valor_0
        return True
    elif pontas[1] == valor_0:
        mesa.append(pedra)
        pontas[1] = valor_1
        return True
    elif pontas[1] == valor_1:
        mesa.append(pedra)
        pontas[1] = valor_0
        return True
    return False


def joga_computador(mao, pontas):
    for i in range(len(mao)):
        pedra = mao[i]
        if pedra[0] == pontas[0] or pedra[0] == pontas[1]:
            return mao.pop(i)
        elif pedra[1] == pontas[0] or pedra[1] == pontas[1]:
            return mao.pop(i)

    return mao.pop()


pedras = cria_domino()
mistura(pedras)


jogar1 = distribui(pedras, 14)
jogar2 = distribui(pedras, 14)

mesa = []
pontas = [-1, -1]
vez_usuario = True
while(len(jogar1) > 0 and len(jogar2) > 0):
    print("MESA: ", mesa)
    if vez_usuario == True:
        pedra = joga_usuario(jogar1, pontas)
        resp = adiciona_jogada(pedra, mesa, pontas)
        if resp == False:
            jogar1.append(pedra)
    else:
        pedra = joga_computador(jogar2, pontas)
        resp = adiciona_jogada(pedra, mesa, pontas)
        if resp == False:
            jogar2.append(pedra)

    vez_usuario = not vez_usuario
