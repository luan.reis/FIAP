import fila
from pilha import Pilha


def criaLabirinto(dimLin, dimCol):
    matriz = []
    for i in range(dimLin):
        matriz.append(['-'] * dimCol)

    matriz[0][2] = matriz[0][3] = '#'
    matriz[1][1] = matriz[1][4] = matriz[1][6] = matriz[1][8] = matriz[1][9] = '#'
    matriz[2][2] = matriz[2][4] = matriz[2][6] = matriz[2][10] = '#'
    matriz[3][1] = matriz[3][3] = matriz[3][7] = matriz[3][8] = '#'
    matriz[4][3] = matriz[4][5] = matriz[4][9] = '#'
    matriz[5][1] = matriz[5][8] = '#'

    return matriz


mat = criaLabirinto(6, 11)
lista = []

mat[0][0] = 100
fila.put(lista, [0, 0])

while not fila.isEmpty(lista):
    pos = fila.get(lista)
    lin = pos[0]
    col = pos[1]

    # encontrar vizinhos livres
    # norte
    if lin - 1 >= 0 and mat[lin-1][col] == '-':
        mat[lin-1][col] = mat[lin][col] + 1
        fila.put(lista, [lin-1, col])

    # sul
    if lin + 1 <= 5 and mat[lin+1][col] == '-':
        mat[lin+1][col] = mat[lin][col] + 1
        fila.put(lista, [lin+1, col])

    # leste
    if col + 1 <= 10 and mat[lin][col+1] == '-':
        mat[lin][col+1] = mat[lin][col] + 1
        fila.put(lista, [lin, col+1])

    # oeste
    if col - 1 >= 0 and mat[lin][col-1] == '-':
        mat[lin][col-1] = mat[lin][col] + 1
        fila.put(lista, [lin, col-1])


Pilha
Pilha.put(lista, [5, 10])

while not Pilha.isEmpty(lista):
    pos = Pilha.pop(lista)
    lin = pos[5]
    col = pos[10]

    # encontrar vizinhos livres
    # norte
    if lin - 1 >= 0 and mat[lin-1][col] == '-':
        mat[lin-1][col] = mat[lin][col] + 1
        Pilha.put(lista, [lin-1, col])

    # sul
    if lin + 1 <= 5 and mat[lin+1][col] == '-':
        mat[lin+1][col] = mat[lin][col] + 1
        Pilha.put(lista, [lin+1, col])

    # leste
    if col + 1 <= 10 and mat[lin][col+1] == '-':
        mat[lin][col+1] = mat[lin][col] + 1
        Pilha.put(lista, [lin, col+1])

    # oeste
    if col - 1 >= 0 and mat[lin][col-1] == '-':
        mat[lin][col-1] = mat[lin][col] + 1
        Pilha.put(lista, [lin, col-1])




print("Caminho inicial [0, 1]")
for lin in mat:
    print(lin)
print("-" * 50)
