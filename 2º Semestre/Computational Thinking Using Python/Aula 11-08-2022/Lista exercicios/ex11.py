
def imprime_times(campeao, lista):
    for pais in lista:
        if pais != campeao:
            print(campeao, "X" , pais)

paises = ['Alemanha', 'Bélgica', 'Brasil', 'Colômbia', 'Costa Rica', 'França' , 'Holanda' ]

for champion in paises: 
    imprime_times(champion, paises)
