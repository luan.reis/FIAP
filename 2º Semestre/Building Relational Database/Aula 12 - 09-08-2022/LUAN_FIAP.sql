CREATE T_RHSTU_TESTE
(ID NUMBER PRIMARY KEY,
NM VARCHAR(50)
);
COMMIT;

INSERT INTO T_RHSTU_FORMA_PAGAMENTO(NM_FORMA_PAGTO, DS_FORMA_PAGTO, ST_FORMA_PAGTO) VALUES ('DINHEIRO', 'PAGTO EM DINDIN � MTO BOM', 'A');

INSERT INTO T_RHSTU_TESTE (ID, NM) VALUES (4, 'S');

DELETE FROM T_RHSTU_FORMA_PAGAMENTO WHERE ID_FORMA_PAGTO = 2;

UPDATE T_RHSTU_ALUNO
SET ID_FORMA_PAGAMENTO = 1
WHERE ID = 2;

CREATE SEQUENCE SQ_RHTSU_FORMA_PAGAMENTO 
START WITH 1 
    NOCACHE 
    ORDER ;

ROLLBACK

SELECT * FROM T_RHSTU_FORMA_PAGAMENTO