<h1>Aula 02/08/2022</h1>

- Esse semestre será mais puxado que o semestre passado

Tópicos da discpina

- Revisão de ciência de dados;
- Aprendizado Supervisionado e não Supervisionado;
- Redução de Dimensionalidade;
- Métricas de Desempenho;
- Redes Neurais Artificiais;
- Processamento da Linguagem Natural;
- IA Clássica

As aulas serão muitos <b>Hands ON</b>

Iremos utilizar a linguagem em python para utlizarmos biliotecas de IA

<h2>Ferramentas</h2>

- Python
- Scikit Learn
- Keras
- Jupyter
- Google Colab


<h2>Definição de inteligencia artificial</h2>

- Ainda não existe uma boa definição pois ainda não sabemos o que
realmente é inteligência!
“Inteligência é a capacidade de
analisar uma determinada
situação, tomar uma decisão e aprender através da
compreensão do resultado”.

- Entretanto, podemos esboçar algumas ideias:
“IA são sistemas artificiais que aparentam possuir inteligência”.
“IA são sistemas artificias que realizam atividades semelhantes as
atividades cognitivas humanas”.

- Importante: um sistema apresentar Inteligência Artificial não significa que o
sistema possui consciência! IA != consciência
E também ainda não sabemos o que é consciência!


<h2>Tipos de IA</h2>

- Agentes Racionais : procedimentos puramente matemáticos que tentam
solucionar o problema. Exemplos:
Busca Gulosa e Busca A*
Algoritmo MinMax

- Sistemas bioinspirados : procedimentos matemáticos que imitam sistemas
biológicos como redes neurais e evolução. Exemplos:
Redes Neurais Artificiais e Deep Learning
Algoritmos Genéticos

<h2>O que é o Aprendizado de Máquina?</h2>

- O que é o Aprendizado de Máquina?

- Machine Learning é a ciência ou a arte de programar computadores para
que eles consigam aprender dado um conjunto de dados GÉRON, 2017

- Machine Learning é a ciência ou a arte de programar computadores para
que eles consigam aprender dado um conjunto de dados GÉRON, 2017