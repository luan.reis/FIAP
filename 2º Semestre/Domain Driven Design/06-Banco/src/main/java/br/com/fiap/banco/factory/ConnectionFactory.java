package br.com.fiap.banco.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Fábrica de conexões
 * 
 * @author Luan Reis
 */
public class ConnectionFactory {

	/**
	 * Obtem uma conexão com o banco de dados
	 * 
	 * @return connection conexao com o banco de dados
	 */
	public static Connection getConnection() throws ClassNotFoundException, SQLException {

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@oracle.fiap.com.br:1521:orcl", "RM94898",
				"191195");
		System.out.println("Conectado!");

		return conn;
	}

}
