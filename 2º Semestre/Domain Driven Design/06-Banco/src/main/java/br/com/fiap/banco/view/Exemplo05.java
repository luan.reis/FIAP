package br.com.fiap.banco.view;

import java.sql.SQLException;
import java.util.function.Consumer;

import br.com.fiap.banco.dao.FilmeDao;
import br.com.fiap.banco.model.Filme;

public class Exemplo05 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		FilmeDao filmes = new FilmeDao();
		
		
		filmes.listar().forEach(new Consumer<Filme>() {
			public void accept(Filme e) {
				System.out.println(e);
			}
		});
	
		

	}

}
