package br.com.fiap.banco.view;

import java.sql.SQLException;
import java.util.Scanner;

import br.com.fiap.banco.dao.FilmeDao;
import br.com.fiap.exception.IdNotFoundException;

public class Exemplo07 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IdNotFoundException {
			Scanner sc = new Scanner(System.in);
			FilmeDao filme = new FilmeDao();
			
			System.out.println("Digite o id do filme para deletar");
			int id = sc.nextInt();
			
			filme.deletar(id);
			System.out.println("Filme deletado com sucesso");

	}

}
