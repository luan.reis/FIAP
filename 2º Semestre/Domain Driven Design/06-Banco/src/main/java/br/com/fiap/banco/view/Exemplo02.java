package br.com.fiap.banco.view;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.fiap.banco.factory.ConnectionFactory;

public class Exemplo02 {

public static void main(String[] args) {
	
	
	
	try {
		Connection conn = ConnectionFactory.getConnection();
		
		Statement stmt = conn.createStatement();
		
		ResultSet resultado = stmt.executeQuery("select * from tdss_tb_filme order by cd_fime");
		
	while(resultado.next()) {
		
		Integer id = resultado.getInt("cd_fime");
		String nome = resultado.getNString("nm_filme");
		Double duracao = resultado.getDouble("nr_minutos");
		Date data = resultado.getDate("dt_lancamento");
		String genero = resultado.getNString("ds_genero");
		
		
		System.out.println("Filme: " + id + ", " + nome + ", " + duracao + ", " + data + ", " + genero);
		
		
	}
		
		
		
	}catch(ClassNotFoundException e) {
		e.printStackTrace();
	}catch(SQLException e) {
		e.printStackTrace();
	}
}

}
