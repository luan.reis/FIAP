package br.com.fiap.pratica02.view;

import java.util.ArrayList;
import java.util.Scanner;

import br.com.fiap.pratica02.model.Aluno;

public class view {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		ArrayList<Aluno> listaAlunos = new ArrayList<>();

		String nome = null;
		String resp = null;
		int rm = 0;
		

		double mediaGeralNotas = 0;
		double mediaGeralIdade = 0;
		int maiorIdade = 0;
		int menorIdade = 0;
		int opcao = 0;

		do {
			
			System.out.println("Digite o RM do aluno: ");
			rm = sc.nextInt();

			System.out.println("Digite o nome do aluno: ");
			nome = sc.next();

			System.out.println("Informa a idade do " + nome);
			int idade = sc.nextInt();

			double notas[] = new double[2];
			for (int i = 0; i < 2; i++) {

				System.out.println("Informe a " + (i + 1) + " nota do " + nome);
				notas[i] = sc.nextDouble();

			}

			listaAlunos.add(new Aluno(rm, nome, idade, notas));

			System.out.println("x-----------------------------------------------------------------------x");
			System.out.print("Deseja informar mais um aluno? [Y/N]: ");
			resp = sc.next().toUpperCase();

		} while (resp.contains("Y"));
		System.out.println("x-----------------------------------------------------------------------x");
		System.out.println("Inclus�o de alunos finalizada!");
		System.out.println("x-----------------------------------------------------------------------x");

		for (int i = 0; i < listaAlunos.size(); i++) {

			mediaGeralNotas += listaAlunos.get(i).media();
			mediaGeralIdade += listaAlunos.get(i).getIdade();
			maiorIdade = listaAlunos.get(0).getIdade();
			if (listaAlunos.get(i).getIdade() > maiorIdade) {
				maiorIdade = listaAlunos.get(i).getIdade();
			}
			menorIdade = listaAlunos.get(0).getIdade();
			if (listaAlunos.get(i).getIdade() < menorIdade) {
				menorIdade = listaAlunos.get(i).getIdade();
			}

		}

		System.out.println("x-----------------------------------------------------------------------x");
		System.out.println("An�lise da lista de alunos \n");
		System.out.println("I) Total de alunos cadastrados: " + listaAlunos.size());
		System.out.println("II) M�dia geral de notas: " + (mediaGeralNotas / listaAlunos.size()));
		System.out.println("III) M�dia geral de idade: " + (mediaGeralIdade / listaAlunos.size()));
		System.out.println("IV) Maior idade " + maiorIdade);
		System.out.println("V) Menor idade " + menorIdade);
		System.out.println("x-----------------------------------------------------------------------x");

		do {
			System.out.println("x-----------------------------------------------------------------------x");
			System.out.println("Digite uma das op��es abaixo: ");
			System.out.println("1) Imprimir os nomes e m�dias de todos os alunos");
			System.out.println("2) Imprimir apenas os nomes dos alunos que possuem m�dia acima de 6.0");
			System.out.println("3) Imprimir apenas os nomes dos alunos que possuem idade acima de 30 anos");
			System.out.println("4) Excluir os alunos com m�dias menores que 3.0");
			System.out.println("5) Sair do sistema");
			System.out.println("x-----------------------------------------------------------------------x");
			System.out.print("Op��o: ");
			opcao = sc.nextInt();

			switch (opcao) {
			case 1:

				for (Aluno a : listaAlunos) {
					System.out.println("Nome: " + a.getNome() + " M�dia: " + a.media());
				}

				break;
			case 2:

				for (Aluno a : listaAlunos) {
					if (a.media() > 6.0) {
						System.out.println("Nome: " + a.getNome() + " M�dia: " + a.media());
					}
				}

				break;
			case 3:

				for (Aluno a : listaAlunos) {
					if (a.getIdade() > 30) {
						System.out.println("Nome: " + a.getNome() + " Idade: " + a.getIdade());
					}
				}

				break;

			case 4:

				for (int i = 0; i < listaAlunos.size(); i++) {
					if (listaAlunos.get(i).media() < 3.0) {
						listaAlunos.remove(i);
					}
					
				}
				listaAlunos.forEach(c -> System.out.println(c));
				break;

			case 5:
				break;
			default:
				System.out.println("----> Op��o Digitada invalida \n");
				break;
			}

		} while (opcao != 5);
		
		System.out.println("Sistema encerrado!");

	}

}
