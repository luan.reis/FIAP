package br.com.fiap.arquivos.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.arquivos.model.Pokemon;

/**
 * Gravar/Ler as informações do Pokemon em um arquivo
 * @author Thiago Yamamoto
 *
 */
public class PokemonDao {

	/**
	 * Ler os pokemons do arquivo e retonar uma List de Pokemons
	 * @return List<Pokemon>
	 * @throws FileNotFoundException 
	 */
	public List<Pokemon> listar() throws IOException {
		
		//Abrir o inputStream para o arquivo "pokedex.cvs"
		FileReader inputStream = new FileReader("pokedex.csv");
		
		//Instanciar o objeto que lê o arquivo 
		BufferedReader reader = new BufferedReader(inputStream);
		
		//Criar a lista de pokemon
		List<Pokemon> lista = new ArrayList<Pokemon>();
		
		//Ler as linhas do arquivo
		String linha;
		while ((linha = reader.readLine()) != null) {
			//Separa as informações do pokemon
			String[] vetor = linha.split(",");
			//Instanciar um pokemon com os valores
			Pokemon pokemon = new Pokemon(vetor[0], vetor[1], 
					Integer.parseInt(vetor[2]), Boolean.parseBoolean(vetor[3]));
			//Adicionar o pokemon na lista
			lista.add(pokemon);
		}
		
		//Fechar os recursos
		reader.close();
		inputStream.close();
		
		//Retornar a lista de pokemon
		return lista;	
	}
	
	/**
	 * Grava um pokemon no arquivo
	 * @param pokemon
	 * @throws IOException 
	 */
	public void cadastrar(Pokemon pokemon) throws IOException {
		
		//Abrir o outputStream para o arquivo "pokedex.csv"
		FileWriter outputStream = new FileWriter("pokedex.csv", true);
		
		//Instanciar o objeto para escrever em um arquivo
		PrintWriter printer = new PrintWriter(outputStream);
		
		//Escrever os atributos do pokemon no arquivo separados por ","
		// Exemplo: pikachu, eletrico, 22, true
		printer.println(pokemon.getNome() + "," + pokemon.getTipo() + "," + 
				pokemon.getAtaque() + "," + pokemon.isEstagioInicial());
		
		//Fechar as paradas
		printer.close();
		outputStream.close();
		
	}
}

