package br.com.fiap.arquivos.view;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import br.com.fiap.arquivos.dao.PokemonDao;
import br.com.fiap.arquivos.model.Pokemon;

/**
 * Interface com o usuário (Exibir e ler as informações do usuário)
 * @author Thiago
 *
 */
public class ExemploPokemon {

	//Cadastrar e listar os pokemons
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		PokemonDao dao = new PokemonDao();
		
		int opcao;
		do {
			System.out.println("Escolha 1-Cadastrar 2-Listar 0-Sair");
			opcao = sc.nextInt();
			switch(opcao) {
				case 1:
					inclusao(sc, dao);
					break;
				case 2:
					exibir(dao);
					break;
			}//switch
		} while (opcao != 0);//while
		
		sc.close();
	}//main

	private static void exibir(PokemonDao dao) {
		try {
			//Obter a lista de pokemons
			//List<Pokemon> lista = dao.listar();
			
			//for para printar a lista
			for (Pokemon p : dao.listar()) {
				System.out.println(p);
			}
			
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private static void inclusao(Scanner sc, PokemonDao dao) {
		//Ler os dados do pokemon (nome, tipo, ataque, estagioInicial)
		System.out.println("Digite o nome do pokemon");
		String nome = sc.next() + sc.nextLine();
		System.out.println("Digite o tipo do pokemon");
		String tipo = sc.next() + sc.nextLine();
		System.out.println("Digite o valor do ataque");
		int ataque = sc.nextInt();
		System.out.println("Estagio inicial? true/false");
		boolean estagioInicial = sc.nextBoolean();
		
		//Instancia o pokemon com os dados
		Pokemon pokemon = new Pokemon(nome, tipo, ataque, estagioInicial);
		
		//Gravar/Cadastrar as informacoes do pokemon no arquivo
		try {
			dao.cadastrar(pokemon);
			
			System.out.println("Pokemon cadastrado!");
		} catch (IOException e) {
			System.err.println("Erro.. nao foi possivel cadastrar o pokemon");
		}
	}
}//class


