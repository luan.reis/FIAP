package br.com.fiap.exceptions.exception;

//Criando uma exception: Criar uma classe que herda de exception
public class XpNegativoException extends Exception {

	public XpNegativoException(String msg) {
		super(msg);
	}
	
}