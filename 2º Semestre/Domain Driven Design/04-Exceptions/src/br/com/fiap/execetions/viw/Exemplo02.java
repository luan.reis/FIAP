package br.com.fiap.execetions.viw;

import java.util.Scanner;

import br.com.fiap.execetions.model.Pokemon;

public class Exemplo02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String resp;
		
		
		System.out.print("Qual o nome do pokemon: ");
		String nome = sc.next() + sc.nextLine();
		
		
		do {
			
			System.out.print("Qual a quantidade de xp que o pokemon vai ganhar: ");
			int xp = sc.nextInt();
			
			Pokemon pokemon = new Pokemon();
			pokemon.setNome(nome);
			
			
			pokemon.evoluir(xp);
			
			System.out.println("Caracteristicas do pokemon: " + pokemon.getNome());
			System.out.println("Level: " + pokemon.getLevel());
			System.out.println("XP atual: " + pokemon.getXp());
			
			System.out.println("Deseja continuar? ");
			 resp = sc.next();
		}while(resp.equalsIgnoreCase("Y"));
		
		
		
	}
	
	
}
