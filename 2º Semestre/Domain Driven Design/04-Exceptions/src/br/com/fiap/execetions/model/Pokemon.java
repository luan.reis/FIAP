package br.com.fiap.execetions.model;

public class Pokemon {
	
	private String nome;
	private int level;
	private int ataque;
	private int xp;
	private String tipo;

	
	
	public void evoluir (int xp) {
		
		this.xp += xp;
		
		if(this.xp > 25) {
			level += 1;
			this.xp = xp - 25;; 
		}
		
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getAtaque() {
		return ataque;
	}
	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}
	
	
	

}
