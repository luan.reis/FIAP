package br.com.fiap.view;

import java.util.ArrayList;

import br.com.fiap.model.Filme;

public class ExemploListaFilmes {
	
	public static void main(String[] args) {
		
		
		ArrayList<Filme> filme =  new ArrayList<>();
		
		
		
		filme.add(new Filme("Harry Potter", 102, "Aventura"));
		filme.add(new Filme("YugiOh", 232, "Sei la"));
		filme.add(new Filme("Se eu fosse voce", 232, "Romance"));
		
		
		filme.forEach(n -> System.out.println(n));
		
	}

}
