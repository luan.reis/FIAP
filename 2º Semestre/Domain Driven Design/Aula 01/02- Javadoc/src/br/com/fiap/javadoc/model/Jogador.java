package br.com.fiap.javadoc.model;

/**
 * Clase que representa um jogador de futebol
 * @author Luans
 * @version 1.0
 *
 */

public class Jogador {

	
	/**
	 * Armazena o nome do jogador
	 */
	
	private String nome;
	
	/**
	 * Armazena o numero do jogador
	 */
	
	private int numero;
	
	/**
	 * Construtor padr�o que recebe parametros (nome do jogador e o numero do jogador)
	 */
	public Jogador() {
		
	}


	/**
	 * Construtor que recebe o nome e o numero da camiseta do jogador
	 * @param nome do jogador
	 * @param numero do jogador
	 */
	public Jogador(String nome, int numero) {
		super();
		this.nome = nome;
		this.numero = numero;
	}

	
	/**
	 * pega o nome do jogador pelo method getNome
	 * @return
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * altera o nome do jogador pelo metodo setNome. recebendo uma string com um novo nome
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	/**
	 * pegar o numero do jogador pelo metodo getNumero
	 * @return
	 */
	public int getNumero() {
		return numero;
	}

	
	/**
	 * Altera o numero do jogador recebendo um parametro de um int.
	 * @param numero
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	
	
	
}
