package br.com.fiap.javadoc.model;

/**
 * Representa uma sele��o da copa do mundo da fifa 2022
 * @author LuanReis
 */

public class Selecao {
	private String nome;
	private Jogador jogador[];

	
	
	
/**
 * Construtor padr�o da classe sele��o	
 */
	public Selecao() {
		
	}
	
	
	/**
	 * Construtor com parametros. que receber� um array de jogadores e o nome do pais
	 * @param jogador
	 * @param nome
	 */
	public Selecao(String nome, Jogador[] jogador) {
		super();
		this.jogador = jogador;
		this.nome = nome;
	}


	
	/**
	 * Metodo que pegar� o array de jogadores da sele��o;
	 * @return
	 */
	public Jogador[] getJogador() {
		return jogador;
	}

	/**
	 * Metodo que altera um array de jogadores da sele��o.
	 * @param jogador
	 */
	
	public void setJogador(Jogador[] jogador) {
		this.jogador = jogador;
	}


	/**
	 * Metodo que pegar� o nome da sele��o que no caso ser� o pa�s;
	 * @return
	 */
	public String getNome() {
		return nome;
	}


	/**
	 * Metodo que altera o nome da sele��o. que receber� um parametro com uma string do nome da sele��o.
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
	
	
	
}
