package br.com.pratica07;

import javax.swing.JOptionPane;

public class Viewer {

	public static void main(String[] args) {

//		Preencha um vetor do tipo double de 16 posições
//		utilizando os valores fornecidos pelo usuário e
//		depois troque os 8 primeiros valores pelos 8
//		últimos e vice-e-versa. Apresente ao final o vetor
//		obtido.
		
		
		double numeros[] = new double[16];
		double aux = 0;
		
		
		
		
		for(int i = 0; i < numeros.length; i++) {
			numeros[i] = Double.parseDouble(JOptionPane.showInputDialog("Informe o " + (i + 1) + " valor: "));		
//			numeros[i] = (i + 1); // para testar habilite aq
			
		}
				
		for(double num : numeros) {
			System.out.println(num);
		}
		
		System.out.println("-----------------------");
		
		for(int j = 0; j < ((numeros.length / 2) - 1); j++) {
			for(int k = (numeros.length - 1); k > 8 ; k--) {
				aux = numeros[j];
				numeros[j] = numeros[k];
				numeros[k] = aux;
				
			}			
		}
	
		
		for(double num : numeros) {
			System.out.println(num);
		}
		
		


	}

}
