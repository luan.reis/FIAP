package br.com.pratica04.view;

import javax.swing.JOptionPane;

public class Viewer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		Faça um programa em Java que leia a idade de N
//		pessoas e informe a sua classe eleitoral:
//		• não eleitor (abaixo de 16 anos)
//		• eleitor obrigatório (entre 18 e 65 anos)
//		• eleitor facultativo (16 ou 17 anos ou maior de
//		65 anos)
//		Ao final da execução exibir o total de eleitores
//		obrigatórios. Nota: o número N é um valor solicitado
//		ao usuário durante a execução do programa; utilize
//		vetores na solução.

		int quantidade = Integer.parseInt(JOptionPane.showInputDialog("Informe a quantidade de pessoas"));
		int idade[] = new int[quantidade];
		int qtdObrigatorio = 0;

		for (int i = 0; i < quantidade; i++) {
			idade[i] = Integer.parseInt(JOptionPane.showInputDialog("Informe a idade da pessoa"));

			if (idade[i] < 16) {

			} else if (idade[i] >= 18 && idade[i] <= 65) {
				qtdObrigatorio += 1;
			} else {

			}

		}

		JOptionPane.showMessageDialog(null, "Quantidade de pessoas que o voto é obrigátorio: " + qtdObrigatorio);

	}

}
