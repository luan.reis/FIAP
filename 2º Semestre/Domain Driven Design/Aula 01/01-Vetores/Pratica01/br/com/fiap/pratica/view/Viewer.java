package br.com.fiap.pratica.view;

import java.util.Scanner;

import javax.swing.JOptionPane;

import br.com.fiap.pratica.model.Produto;

public class Viewer {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// TODO Auto-generated method stub
		int quantidade = Integer.parseInt(JOptionPane.showInputDialog("Informe a quantidade de produtos que deseja cadastrar"));
		Produto[] produtos = new Produto[quantidade];
		
		String[] array = new String[quantidade];
		
		for(int i = 0; i < quantidade; i++) {
			
			int quantidadeProd = Integer.parseInt(JOptionPane.showInputDialog("Informe a quantidade do produto " + (i + 1) ));
			double preco = Double.parseDouble(JOptionPane.showInputDialog("Informe o preço do produto " + (i + 1) ));
			double desconto = Double.parseDouble(JOptionPane.showInputDialog("Informe o desconto do produto " + (i + 1) ));
			
		
			produtos[i] = new Produto(quantidadeProd, preco, desconto);
		
		}		
		
		for(int i = 0; i < produtos.length; i++) {
			System.out.println(produtos[i].toString());
		}

	}

}
