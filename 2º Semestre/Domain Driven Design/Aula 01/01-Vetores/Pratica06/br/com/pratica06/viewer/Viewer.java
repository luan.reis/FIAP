package br.com.pratica06.viewer;

import javax.swing.JOptionPane;

public class Viewer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		Crie um programa em Java que preencha as N
//		posições de um vetor do tipo short com os N
//		primeiros números ímpares. Ao final exiba os
//		valores do vetor na tela. Nota: o número N é um
//		valor solicitado ao usuário durante à execução do
//		programa

		int quantidade = Integer.parseInt(JOptionPane.showInputDialog("Quantidade de numeros"));
		short numeroDig = 0;
		short numeros[] = new short[quantidade];

		for (int i = 0; i < quantidade; i++) {
			numeroDig = Short.parseShort(JOptionPane.showInputDialog("Informe o " + (i + 1) + " numero:"));
			if (numeroDig % 2 != 0) {
				numeros[i] = numeroDig;

			}
		}

		for (short num : numeros) {
			if (num == 0) {

			} else {
				System.out.println(num);

			}

		}

	}

}
