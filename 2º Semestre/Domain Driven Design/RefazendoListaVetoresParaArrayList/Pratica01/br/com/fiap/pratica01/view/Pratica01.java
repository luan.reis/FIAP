package br.com.fiap.pratica01.view;

import java.util.ArrayList;
import java.util.Scanner;

import br.com.fiap.pratica01.model.Produtos;

public class Pratica01 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		ArrayList<Produtos> produto = new ArrayList<>();
		
		System.out.println("Quantos produtos v�o ser inseridos? ");
		int qtdProd = sc.nextInt();
		
		for (int i = 0; i < qtdProd; i++) {
			
			System.out.println("Digite o nome do produto: ");
			String nome = sc.next();
			System.out.println("Digite a categoria: ");
			String categoria = sc.next();
			System.out.println("Digite o valor do produto");
			double valor = sc.nextDouble();
			System.out.println("Informe a quantidade de produtos");
			int quantidade = sc.nextInt();
			
			produto.add(new Produtos(nome, categoria, valor, quantidade));
			
		}
		
		produto.forEach(c -> System.out.println(c));

	}

}
