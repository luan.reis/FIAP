package br.com.fiap.pratica06.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class view {

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);
		ArrayList<Short> nums = new ArrayList<>();
		ArrayList<Short> pares = new ArrayList<>();
		ArrayList<Short> impares = new ArrayList<>();
		
		System.out.println("Digite a quantidade de numeros que vai inserir");
		int quantidade = sc.nextInt();
		
		for (int i = 0; i < quantidade; i++) {
			
			System.out.println("Digite o " + (i + 1) + " numero:");
			nums.add(sc.nextShort());
			
		}
		
		for (int i = 0; i < nums.size(); i++) {
			
			if(nums.get(i) % 2 != 0) {
				impares.add(nums.get(i));
			}else {
				pares.add(nums.get(i));
				
			}
			
		}
		
		nums.clear();
		Collections.sort(impares);
		nums.addAll(impares);
		Collections.sort(pares);
		nums.addAll(pares);
		
		System.out.println("-----------------------------------------------");
		System.out.println(nums);
		
		
		
		
		
		
	}

}
