package br.com.fiap.pratica03.view;

import java.util.ArrayList;
import java.util.Scanner;

import br.com.fiap.pratica03.model.Aluno;

public class view {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String nome = null;
		ArrayList<Aluno> aluno = new ArrayList<>();
		ArrayList<Double> nota;
		
		System.out.println("Informe a quantidade de alunos: ");
		int quantidade = sc.nextInt();
		
		
		for (int i = 0; i < quantidade; i++) {
			System.out.println("Informe o nome do aluno: ");	
			nome = sc.next();
			
			nota = new ArrayList<>();
			for (int j = 0; j < 3; j++) {
				System.out.println("Digite a " + (j + 1) +" Nota");
				nota.add(sc.nextDouble());
				
			}
			aluno.add(new Aluno(nome, nota));
			
		}
		
		aluno.forEach(a -> System.out.println(a));
		
		
		

	}

}
