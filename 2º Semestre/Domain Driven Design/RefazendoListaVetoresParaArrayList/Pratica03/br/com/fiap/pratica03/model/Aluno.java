package br.com.fiap.pratica03.model;

import java.util.ArrayList;

public class Aluno {

	private String nome;

	ArrayList<Double> notas = new ArrayList<>();

	public Aluno(String nome, ArrayList<Double> notas) {
		super();
		this.nome = nome;
		this.notas = notas;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<Double> getNotas() {
		return notas;
	}

	public void setNotas(ArrayList<Double> notas) {
		this.notas = notas;
	}

	private double media() {

		double media = 0;

		for (Double nota : notas) {

			media += nota;

		}

		return media = media / notas.size();
		
	}

	@Override
	public String toString() {
		return "Aluno [Nome: " + nome + ", Notas: "  +notas + " Media: " + media() + "]";
	}

}
