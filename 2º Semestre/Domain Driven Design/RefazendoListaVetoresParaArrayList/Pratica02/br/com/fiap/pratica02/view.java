package br.com.fiap.pratica02;

import java.util.ArrayList;
import java.util.Scanner;

public class view {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		ArrayList<Integer> num = new ArrayList<>();

		System.out.println("Digite a quantidade de numeros que ser�o inseridos");
		int quantidade = sc.nextInt();

		for (int i = 0; i < quantidade; i++) {

			System.out.println("Digite o " + (i + 1) + " valor");
			num.add(sc.nextInt());
		}
		
		int maior = 0;
		for (Integer i : num) {
			
			if(i > maior) {
				maior = i;
			}
			
		}
		
		System.out.println("------------------------------------------");
		System.out.println("Maior numero digitado: " + maior);
		

		

	}

}
