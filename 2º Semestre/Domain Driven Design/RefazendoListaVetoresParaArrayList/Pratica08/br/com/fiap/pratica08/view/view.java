package br.com.fiap.pratica08.view;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class view {

	public static void main(String[] args) {
//		Faça um programa que leia um vetor de char com
//		30 posições e verifique se existem valores
//		repetidos, caso exista apresente-os na tela.
		
		Scanner sc = new Scanner(System.in);
		ArrayList<Character> chars = new ArrayList<>();
		Set<Character> repetidos = new HashSet<>();
		
		char[] letters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Z' , 'A', 'B', 'C', 'D', 'E', 'F'};
		
		for (int i = 0; i < 30; i++) {
			
			if(chars.contains(letters[i])) {
				repetidos.add(letters[i]);
				
			}
			chars.add(letters[i]);
		}
		
		System.out.println("Caracteres repitidos");
		repetidos.forEach(c -> System.out.println(c));
		
		
	}
	
}
