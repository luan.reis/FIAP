package br.com.fiap.pratica04.view;

import java.util.ArrayList;
import java.util.Scanner;

import br.com.fiap.pratica04.model.Eleitor;

public class view {

	public static void main(String[] args) {
		ArrayList<Eleitor> eleitor = new ArrayList<>();
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Quantidade de eleitores: ");
		int quantidade = sc.nextInt();
		
		String nome = null;
		int idade = 0;
		int qtdEleitoresObrigatorios = 0;
		
		for (int i = 0; i < quantidade; i++) {
			
			System.out.println("Digite o nome do eleitor");
			nome = sc.next();
			
			System.out.println("Digite a idade do " + nome);
			idade = sc.nextInt();
			
			eleitor.add(new Eleitor(nome, idade));
			
		}
		
		for (Eleitor e : eleitor) {
			
			if(e.verificaObrigatoriedade() == true) {
				qtdEleitoresObrigatorios += 1;
			}
			
		}
		
		System.out.println("-------------------------------------------------------------");
		System.out.println("Quantidade de eleitores com voto obrigatorios: " + qtdEleitoresObrigatorios);
		System.out.println("-------------------------------------------------------------");
		eleitor.forEach(c -> System.out.println(c));
	}

}
