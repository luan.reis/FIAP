package br.com.fiap.pratica04.model;

public class Eleitor {

	private String nome;
	private int idade;
	private boolean obrigatoriedadeVoto;
	
	
	
	
	public Eleitor(String nome, int idade) {
		super();
		this.nome = nome;
		this.idade = idade;
	}




	public String getNome() {
		return nome;
	}




	public void setNome(String nome) {
		this.nome = nome;
	}




	public int getIdade() {
		return idade;
	}




	public void setIdade(int idade) {
		this.idade = idade;
	}




	public boolean isObrigatoriedadeVoto() {
		return obrigatoriedadeVoto;
	}




	public void setObrigatoriedadeVoto(boolean obrigatoriedadeVoto) {
		this.obrigatoriedadeVoto = obrigatoriedadeVoto;
	}



	public boolean verificaObrigatoriedade() {
		
		boolean obrigatoriedade;
		
		if(idade < 16) {
			obrigatoriedadeVoto = false;
		}else if(idade >= 18 && idade <= 65) {
			obrigatoriedadeVoto = true;
		}else {
			obrigatoriedadeVoto = false;
		}
		
		return obrigatoriedadeVoto;
		
	}
	

	@Override
	public String toString() {
		return "Eleitor [Nome: " + nome + ", Idade: " + idade + ", Obrigatorio votar: " + verificaObrigatoriedade() + "]";
	}
	
	
	
	
	
	

}
